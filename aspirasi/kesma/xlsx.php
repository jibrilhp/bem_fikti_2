<?php
require_once 'login.php';
require_once '../db.php';
$resultSeminar=$mysqli->query("SELECT * FROM aspirasi ORDER BY jurusan ASC");

// Inisialisasi object PHPEXcel
require_once 'Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel(); 

// Menentukan sheet yang aktif di sheet 0
$objPHPExcel->setActiveSheetIndex(0);

//header
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Data Aspirasi Mahasiswa');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//judul kolom
$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'No.');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Nama');
$objPHPExcel->getActiveSheet()->SetCellValue('C2', 'Kelas');
$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'NPM');
$objPHPExcel->getActiveSheet()->SetCellValue('E2', 'Email');
$objPHPExcel->getActiveSheet()->SetCellValue('F2', 'Jurusan');
$objPHPExcel->getActiveSheet()->SetCellValue('G2', 'Kategori');
$objPHPExcel->getActiveSheet()->SetCellValue('H2', 'Kritik');
$objPHPExcel->getActiveSheet()->SetCellValue('I2', 'Saran');

//ubah lebar kolom B -> J
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

// Nilai awal row
$baris = 3;
$urut = 1;

// Iterate through each result from the SQL query in turn
// We fetch each database result row into $row in turn
while($row = $resultSeminar->fetch_array()){ 
    // Set cell An to the "name" column from the database (assuming you have a column called name)
    // where n is the Excel row number (ie cell A3 in the first row)
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$baris, $urut);
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$baris, $row['nama']);
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$baris, $row['kelas']);
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$baris, $row['npm']);
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$baris, $row['email']);
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$baris, $row['jurusan']);
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$baris, $row['kategori']);
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$baris, $row['kritik']);
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$baris, $row['saran']);
    
    // Increment the Excel row counter
    $baris++;
    $urut++;
}
//ubah style header
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

// Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
// Write the Excel file to filename some_excel_file.xlsx in the current directory

// Write file to the browser
$objWriter->save('Daftar_Aspirasi_Mahasiswa.xlsx'); 
//$objWriter->save('php://output');


// We'll be outputting an excel file
$file = "Daftar_Aspirasi_Mahasiswa.xlsx";

header('Content-disposition: attachment; filename='.$file);
header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Length: ' . filesize($file));
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
ob_clean();
flush(); 
readfile($file);
?>
