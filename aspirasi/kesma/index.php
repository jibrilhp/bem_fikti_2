<?php
require_once 'login.php';
require_once '../db.php';
$result=$mysqli->query("SELECT * FROM aspirasi");

//output db ke table
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Form Aspirasi BEM FIKTI UG </title>
<meta name="Description" content="Sampaikan Aspirasimu Mengenai Fakultas Ilmu Komputer dan Teknologi Informasi Universitas Gunadarma di Sini!">
<meta property="og:type" content="website">
<meta property="og:title" content="Form Aspirasi BEM FIKTI UG">
<meta property="og:description" content="Sampaikan Aspirasimu Mengenai Fakultas Ilmu Komputer dan Teknologi Informasi Universitas Gunadarma di Sini!">
<link href='http://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet' type='text/css'>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.semanticui.min.css">
<style>
body {
    color: #3F3F3F;
    font-family:'Droid Sans', Tahoma, Arial, Verdana sans-serif;
    background: url(bg.png) no-repeat fixed;
}
</style>
</head>
<body>
<div style="max-width: 1000px; margin: 75px auto 50px auto; background-color: #FFF6; padding: 30px; border-radius: 15px;">
    <center><h1>Aspirasi Mahasiswa FIKTI Universitas Gunadarma</h1></center><br/>
    <center><h3><a href="xlsx.php">Download Data Spreadsheet</a></h3></center><br/>
    <center><h3><a href="?logout">Logout</a></h3></center>
    <table id="example" class="ui celled table stripe" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>NPM</th>
                <th>Email</th>
                <th>Jurusan</th>
                <th>Kategori</th>
                <th>Kritik</th>
                <th>Saran</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>NPM</th>
                <th>Email</th>
                <th>Jurusan</th>
                <th>Kategori</th>
                <th>Kritik</th>
                <th>Saran</th>
            </tr>
        </tfoot>
        <tbody>
<?php
$no=0;
while($row = $result->fetch_array()){
$no++;
echo('<tr>
        <td>'.$no.'</td>
        <td>'.$row['nama'].'</td>
        <td>'.$row['kelas'].'</td>
        <td>'.$row['npm'].'</td>
        <td>'.$row['email'].'</td>
        <td>'.$row['jurusan'].'</td>
        <td>'.$row['kategori'].'</td>
        <td>'.$row['kritik'].'</td>
        <td>'.$row['saran'].'</td>
        <td>' .$row['tanggal'] . '</td>
        </tr>');
}
?>
        </tbody>
    </table>
</div>
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js"></script>
<script type="text/javascript" class="init">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>
