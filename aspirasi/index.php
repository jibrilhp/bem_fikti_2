<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Form Aspirasi BEM FIKTI UG</title>
<meta name="Description" content="Sampaikan Aspirasimu Mengenai Fakultas Ilmu Komputer dan Teknologi Informasi Universitas Gunadarma di Sini!">
<meta property="og:type" content="website">
<meta property="og:title" content="Form Aspirasi BEM FIKTI UG">
<meta property="og:description" content="Sampaikan Aspirasimu Mengenai Fakultas Ilmu Komputer dan Teknologi Informasi Universitas Gunadarma di Sini!">
<link href='http://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet' type='text/css'>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<?php if(isset($_POST['uhuy'])){
    require_once 'db.php';
	require_once 'g_verify.php';
	
	if (cek_captcha($_POST["g-recaptcha-response"]) !== true) {
		echo "<script>alert('silahkan konfirmasi captcha terlebih dahulu');  window.history.back();</script>";
		
		die();
	}
		
	
    $nama = $_POST['nama'];
    $npm = $_POST['npm'];
    $kelas = $_POST['kelas'];
    $email = $_POST['email'];
    $jurusan = $_POST['jurusan'];
    $kategori = $_POST['kategori'];
    $kritik = $_POST['kritik'];
    $saran = $_POST['saran'];
    $tgl = date ("d-M-Y H:i:s");
    $query = $mysqli->prepare('INSERT INTO aspirasi (nama,kelas,npm,email,jurusan,kategori,kritik,saran,tanggal)VALUES(?,?,?,?,?,?,?,?,?)');
    $query->bind_param('sssssssss', $nama,$kelas,$npm,$email,$jurusan,$kategori,$kritik,$saran,$tgl);
    if (!$query->execute()) {
        die("kesalahan terjadi!");
    }
    $query->close();
?>
    <div id="content">
        <h1>Form Aspirasi Mahasiswa/i FIKTI Universitas Gunadarma</h1>

        <form action="" method="post" autocomplete="on" style="padding: 30px 0;">
        Terimakasih, aspirasimu telah kami terima :)
        </form>
    </div>
    
<?php
	include ('./mail/PHPMailerAutoload.php');
$mail = new PHPMailer;
                      

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'localhost;hosting.gunadarma.ac.id';  			  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'bemfikti@gunadarma.ac.id';           // SMTP username
	$mail->Password = '#KolaborasiBermanfaat';                          // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                          
	
	$mail->setFrom('bemfikti@gunadarma.ac.id', 'Aspirasi UG BEM FIKTI');
	$mail->addAddress('bemfikti.ug@gmail.com', 'Biro HUMAS');     // Add a recipient
	$mail->addAddress('jibrilhp@outlook.com', 'Admin');     // Add a recipient
	
	
	$mail->isHTML(true);   
	
	$mail->Subject = 'Permintaan aspirasi '.$nama;
	$mail->Body    = '<table border="1" style="border-collapse: collapse; border: 1px solid black; text-align: left;">
<tr style="border: 1px solid black; background: #00557F; color: #FFFFFF;"><th colspan="2" style="border: 1px solid black; text-align: center;">Data aspirasi mahasiswa.</th></tr>
<tr style="border: 1px solid black;"><td style="border: 1px solid black;">Nama yang mengajukan: </td><td>'.$nama.'</td></tr>
<tr style="border: 1px solid black; background: #E1EEf4; color: #00557F;"><td style="border: 1px solid black;">Kelas: </td><td>'.$kelas.'</td></tr>
<tr style="border: 1px solid black;"><td style="border: 1px solid black;">Kritik: </td><td>'.$kritik.'</td></tr>
<tr style="border: 1px solid black;"><td style="border: 1px solid black;">Saran: </td><td>'.$saran.'</td></tr>
</table>';
if(!$mail->send()) {
    	echo 'Message could not be sent.';
    	echo 'Mailer Error: ' . $mail->ErrorInfo;
		die();
	} 

 }else{ ?>
    <div id="content">
        <h1>Form Aspirasi Mahasiswa/i FIKTI Gunadarma</h1>

        <form action="" method="post" autocomplete="on">
            <p>
                <label for="nama" class="fa fa-user"> Nama<span class="required">*</span></label>
                <input type="text" name="nama" id="nama" required="required" placeholder="Nama Lengkap" />
            </p>

            <p>
                <label for="kelas" class="fa fa-id-badge"> Kelas
                    <span class="required">*</span>
                </label>
                <input type="text" name="kelas" id="kelas" required="required" placeholder="Kelas" />
            </p>

            <p>
                <label for="npm" class="fa fa-id-card"> NPM<span class="required">*</span></label>
                <input type="text" name="npm" id="npm" required="required" placeholder="NPM" />
            </p>

            <p>
                <label for="email" class="fa fa-envelope"> E-mail address<span class="required">*</span></label>
                <input type="email" name="email" id="email" placeholder="Email" required="required" />
            </p>

            <p>
                <label for="jurusan" class="fa fa-graduation-cap"> Jurusan<span class="required">*</span></label>
                <select name="jurusan" id="jurusan" required="">
                    <option value="" disabled selected>Jurusan Anda</option>
                    <option value="Sistem Informasi">Sistem Informasi</option>
                    <option value="Sistem Komputer">Sistem Komputer</option>
                </select>
            </p>

            <p>
                <label for="subject" class="fa fa-bullhorn"> Kategori<span class="required">*</span></label>
                <select name="kategori" id="kategori" required="">
                    <option value="" disabled selected>Pilih Kategori</option>
                    <option value="Fasilitas Kampus">Fasilitas Kampus</option>
                    <option value="Aspirasi Organisasi">Aspirasi Organisasi</option>
                    <option value="Administrasi Kampus">Administrasi Kampus</option>
                    <option value="Kegiatan Belajar Mengajar">Kegiatan Belajar Mengajar</option>
                </select>
            </p>

            <p>
                <label for="kritik" class="fa fa-comment-o"> Kritik<span class="required">*</span></label>
                <textarea placeholder="Sampaikan kritik Anda" name="kritik" required="required"></textarea>
            </p>

            <p>
                <label for="kritik" class="fa fa-comments-o"> Saran<span class="required">*</span></label>
                <textarea placeholder="Sampaikan saran Anda" name="saran" required="required"></textarea>
            </p>
			<p>
			<div class="g-recaptcha" data-sitekey="6LfHqkEUAAAAAB0QdOVCFtk2JJD9ngDXKCBPFdPj"></div>
			</p>
            <p class="indication"><span class="required">*</span>Jangan lupa mengisi yang belum terisi.</p>
            <div style="min-height: 42px;">
                <input type="submit" name="uhuy" value="Sampaikan Aspirasimu !" />
				
            </div>
        </form>
    </div>
<?php } ?>
</body>
</html>
