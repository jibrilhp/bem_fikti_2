<?php
if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
?>

<!DOCTYPE html>
<head>
	<title>Form Peminjaman Proyektor BEM FIKTI UG</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<script>
  	$(function(){
    	$("#datepickerint").datepicker({
  		dateFormat: "dd-mm-yy"
		});
  	});
  	$(function(){
    	$("#datepickerext").datepicker({
  		dateFormat: "dd-mm-yy"
		});
  	});
  	</script>
	<link rel="stylesheet" href="css/demo.css">
	<link rel="stylesheet" href="css/sky-forms.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/sky-forms-ie8.css">
	<![endif]-->
	
	<!--[if lt IE 10]>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="js/jquery.placeholder.min.js"></script>
	<![endif]-->		
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/sky-forms-ie8.js"></script>
	<![endif]-->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<?php

if(isset($_POST['sewa'])) {
	include 'db.php';
	include ('lib/mail/PHPMailerAutoload.php');

	require_once 'g_verify.php';
	
	if (cek_captcha($_POST["g-recaptcha-response"]) !== true) {
		echo "<script>alert('silahkan konfirmasi captcha terlebih dahulu');  window.history.back();</script>";
		
		die();
	}
	
    $string = '';
    $random_string_length = 6;
    $characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
    $max = strlen($characters) - 1;
    for ($i = 0; $i < $random_string_length; $i++) {
        $string .= $characters[mt_rand(0, $max)];
    }
    $kode =  $string;
	$nama = $_POST['nama'];
	$npm = $_POST['npm'];
	$email = $_POST['email'];
	$type = $_POST['type'];
	$divisi = $_POST['divisi'];
	$keperluan = $_POST['keperluan'];
	$jumlah = $_POST['jumlah'];
	$tgl = $_POST['tgl'];
	$durasi = $_POST['durasi'];
	$biaya = $_POST['biaya'];
	$tglback = $_POST['tglback'];

	$query = $mysqli->prepare('INSERT INTO proyektor (kode,nama,npm,email,org,peminjam,keperluan,jumlah,tgl_pinjam,durasi_pinjam,tgl_kembali,biaya)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)');
	$query->bind_param('ssssssssssss', $kode,$nama,$npm,$email,$type,$divisi,$keperluan,$jumlah,$tgl,$durasi,$tglback,$biaya);
	$query->execute();
	$query->close();

	// Include library PHPMailer
	$mail = new PHPMailer;

	if($type == 'Internal'){
		// Email ke peminjam internal
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'localhost;hosting.gunadarma.ac.id';  			  			// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'bemfikti@gunadarma.ac.id';         			// SMTP username
		$mail->Password = '#KolaborasiBermanfaat';                     			// SMTP password
		$mail->SMTPSecure = 'tls';                            			// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    			// TCP port to connect to
		$mail->setFrom('bemfikti@gunadarma.ac.id', 'BEM FIKTI UG');		// Set email sender
		$mail->addAddress($email, $nama);     							// Add a recipient
		$mail->isHTML(true);                                  			// Set email format to HTML
		$mail->Subject = 'Konfirmasi Peminjaman Proyektor BEM FIKTI';	// Set email subject
		ob_start();
		include('content-internal.php');
		$mail->Body = ob_get_contents();
		ob_end_clean();
		$mail->send();													// Kirim email
	}else{
		// Email ke peminjam eksternal
		include 'x-gen-invoice.php';	// Generate invoice pdf
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'localhost;hosting.gunadarma.ac.id';  			  			// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'bemfikti@gunadarma.ac.id';         			// SMTP username
		$mail->Password = '#KolaborasiBermanfaat';                     			// SMTP password
		$mail->SMTPSecure = 'tls';                            			// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    			// TCP port to connect to
		$mail->setFrom('bemfikti@gunadarma.ac.id', 'BEM FIKTI UG');		// Set email sender
		$mail->addAddress($email, $nama);     							// Add a recipient
		$mail->isHTML(true);                               				// Set email format to HTML
		$mail->addAttachment('./invoice/invoice'.$kode.'.pdf', 'Invoice-'.$kode.'.pdf');// Optional name
		$mail->Subject = 'Konfirmasi Peminjaman Proyektor BEM FIKTI';	// Set email subject
		ob_start();
		include('content-eksternal.php');
		$mail->Body = ob_get_contents();
		ob_end_clean();
		$mail->send();													// Kirim email
	}

	// Email ke bendum & kabid4
	$mail->isSMTP();                                      			// Set mailer to use SMTP
	$mail->Host = 'localhost;hosting.gunadarma.ac.id';  			  			// Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               			// Enable SMTP authentication
	$mail->Username = 'bemfikti@gunadarma.ac.id';         			// SMTP username
	$mail->Password = '#KolaborasiBermanfaat';                     			// SMTP password
	$mail->SMTPSecure = 'tls';                            			// Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    			// TCP port to connect to
	$mail->setFrom('bemfikti@gunadarma.ac.id', 'BEM FIKTI UG');		// Set email sender
	if($jumlah == 1){
		$mail->addAddress('Bagkeuanganbemfiktiug@gmail.com', 'Bagian keuangan');	// Add a recipient
		$mail->addAddress('jibrilhp@outlook.com', 'Jibril Hartri Putra');			// Add a recipient
		$mail->addAddress('bemfikti@gunadarma.ac.id', 'Arsip');			// Add a recipient
	}else{
		$mail->addAddress('Bagkeuanganbemfiktiug@gmail.com', 'Bagian Keuangan');	// Add a recipient
		$mail->addAddress('jibrilhp@outlook.com', 'Jibril Hartri Putra');			// Add a recipient
		$mail->addAddress('bemfikti@gunadarma.ac.id', ' Arsip');			// Add a recipient
	}
	$mail->isHTML(true);                                  			// Set email format to HTML
	if($type == 'Eksternal'){
		$mail->addAttachment('./invoice/invoice'.$kode.'.pdf', 'Invoice-'.$kode.'.pdf');// Optional name
	}
	$mail->Subject = 'Peminjaman Proyektor BEM FIKTI';				// Set email subject
	ob_start();
	include('content-bem.php');
	$mail->Body = ob_get_contents();
	ob_end_clean();
	$mail->send();													// Kirim email
?>

<body class="bg-green">
	<?php
	//keperluan analisis
	include ($_SERVER["DOCUMENT_ROOT"] . "/analisis.php");
	?>
		<div class="body body-s">
			<form method="post" name="daftar" action="" class="sky-form">
				<header>Form Peminjaman Proyektor BEM FIKTI UG</header>
				<fieldset>
					<section>
					<h3>Terimakasih, permintaan Anda telah kami terima. Harap segera periksa email Anda dan segera melakukan konfirmasi sesuai dengan cara yang diberikan pada email yang akan Anda terima.</h3>
					</section>
				</fieldset>
			</form>
		</div>
</body>
</html>
<?php
if($type == 'Eksternal'){
	$invoice_del = './invoice/invoice'.$kode.'.pdf';
	unlink($invoice_del);
}
die();
}

if(isset($_GET['ketentuan'])){
?>
<body class="bg-green">
		<div class="body body-s">
			<form method="post" name="daftar" action="" class="sky-form">
				<header>Form Peminjaman Proyektor BEM FIKTI UG</header>
				<fieldset>
					<section>
					<h3>Ketentuan.</h3>
					<p>
					<ol>
						<li>Dengan ini saya menyatakan bahwa data di atas adalah benar.</li>
						<li>Saya berjanji akan menggunakan proyektor dengan bijaksana.</li>
						<li>Saya bersedia bertanggung jawab terhadap peminjaman proyektor sepenuhnya.</li>
						<li>Saya akan mengembalikan secara utuh dan bersih barang yang dipinjam.</li>
						<li>Jika terjadi kerusakan atau hilangnya peralatan yang dipinjam, saya bersedia untuk mengganti peralatan tersebut dengan yang baru atau sesuai dengan kesepakan yang akan disepakati sesudahnya.</li>
						<li>Saya bersedia dikenakan denda sebesar Rp. 50.000/hari apabila peminjaman melebihi batas waktu pengembalian yang telah ditentukan (Max H+1 setelah peminjaman).</li>
					</ol>
					</p>
					</section>
					<section>
					<h3><a href="./">Go Back</a></h3>
					</section>
				</fieldset>
			</form>
		</div>
</body>
</html>
<?php
die();
}
?>


<body class="bg-green">
	<div class="body body-s">
	
		<form method="post" name="daftar" action="?status=success" class="sky-form">
			<header>Form Peminjaman Proyektor BEM FIKTI UG</header>
			
			<fieldset>
			
				<section>
					<label class="input">
						<input type="text" name="nama" id="nama" placeholder="Nama" autocomplete="off" required>
						<b class="tooltip tooltip-top-right">Isikan nama lengkap Anda.</b>
					</label>
				</section>

				<section>
					<label class="input">
						<input type="text" name="npm" id="npm" placeholder="NPM" autocomplete="off" required>
						<b class="tooltip tooltip-top-right">Isikan NPM Anda.</b>
					</label>
				</section>

				<section>
					<label class="input">
						<input type="email" name="email" id="email" placeholder="Email" autocomplete="off" required>
						<b class="tooltip tooltip-top-right">Isikan Email Anda.</b>
					</label>
				</section>

				<section>
				<label class="select">
					<select id="type" name="type" required="">
					<option value="" selected disabled>Status Peminjam</option>
					<option value="Internal">Internal</option>
					<option value="Eksternal">Eksternal</option>
					</select>
					<i></i>
				</label>
				</section>


				<div id="internal" style="display: none;">
					<section>
					<label class="select">
						<select id="divisi" name="divisi" required="">
						<option value="" selected disabled>Departemen / Biro / Bagian</option>
						<option value="Departemen Akademik">Departemen Akademik</option>
						<option value="Departemen Kewirausahaan">Departemen Kewirausahaan</option>
						<option value="Departemen Kesejahteraan Mahasiswa">Departemen Kesejahteraan Mahasiswa</option>
						<option value="Departemen Pengabdian Masyarakat">Departemen Pengabdian Masyarakat</option>
						<option value="Departemen Olahraga">Departemen Olahraga</option>
						<option value="Departemen Seni dan Budaya">Departemen Seni dan Budaya</option>
						<option value="Biro Hubungan Masyarakat">Biro Hubungan Masyarakat</option>
						<option value="Biro Pengembangan Sumber Daya Manusia">Biro Pengembangan Sumber Daya Manusia</option>
						<option value="Biro Media">Biro Media</option>
						<option value="Biro Pengembangan Teknologi Informasi">Biro Pengembangan Teknologi Informasi</option>
						<option value="Bagian Kesekretariatan">Bagian Kesekretariatan</option>
						<option value="Bagian Keuangan">Bagian Keuangan</option>
						</select>
						<i></i>
					</label>
					</section>
					<section>
						<label class="textarea">
							<textarea name="keperluan" id="keperluan" placeholder="Keperluan" autocomplete="off" required></textarea>
							<b class="tooltip tooltip-top-right">Keperluan Anda meminjam.</b>
						</label>
					</section>
					<section>
						<label class="select">
							<select id="jumlah" name="jumlah" required="">
							<option value="" selected disabled>Jumlah Pinjaman</option>
							<option value="1">1 Proyektor</option>
							<option value="2">2 Proyektor</option>
							</select>
							<i></i>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="text" name="tgl" id="datepickerint" placeholder="Tanggal peminjaman" autocomplete="off" required>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="number" min="1" max="5" step="1" name="durasi" id="durasiint" placeholder="Durasi peminjaman (Dalam hari)" autocomplete="off" onkeypress="hitungint();" onkeydown="hitungint();" onkeyup="hitungint();" disabled="" required>
							<input type="hidden" name="biaya" id="biayaint" value="0" required>
							<b class="tooltip tooltip-top-right">Jumlah hari peminjaman (Maks 5 Hari).</b>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="text" name="tglback" id="tglbackint" placeholder="Tanggal pengembalian" required readonly>
							<b class="tooltip tooltip-top-right">Tanggal pengembalian.</b>
						</label>
					</section>
				</div>


				<div id="eksternal" style="display: none;">
					<section>
						<label class="input">
							<input type="text" name="divisi" id="divisi" placeholder="Ormawa" autocomplete="off" required>
							<b class="tooltip tooltip-top-right">Ormawa Anda.</b>
						</label>
					</section>
					<section>
						<label class="textarea">
							<textarea name="keperluan" id="keperluan" placeholder="Keperluan" autocomplete="off" required></textarea>
							<b class="tooltip tooltip-top-right">Keperluan Anda meminjam.</b>
						</label>
					</section>
					<section>
						<label class="select">
							<select id="jumlah" name="jumlah" required="" readonly="">
							<option value="" disabled>Jumlah Pinjaman</option>
							<option value="1" selected>1 Proyektor</option>
							</select>
							<i></i>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="text" name="tgl" id="datepickerext" placeholder="Tanggal peminjaman" autocomplete="off" required />
						</label>
					</section>
					<section>
						<label class="input">
							<input type="number" min="1" max="5" step="1" name="durasi" id="durasiext" placeholder="Durasi peminjaman (Dalam hari)" autocomplete="off" required onkeypress="hitungext();" onkeydown="hitungext();" onkeyup="hitungext();" disabled="">
							<b class="tooltip tooltip-top-right">Jumlah hari peminjaman (Maks 5 Hari).</b>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="text" name="biaya" id="biayaext" placeholder="Biaya" autocomplete="off" required readonly>
							<b class="tooltip tooltip-top-right">Biaya.</b>
							<input type="hidden" name="biaya" id="biayaext" required readonly>
						</label>
					</section>
					<section>
						<label class="input">
							<input type="text" name="tglback" id="tglbackext" placeholder="Tanggal pengembalian" required readonly>
							<b class="tooltip tooltip-top-right">Tanggal pengembalian.</b>
						</label>
					</section>
					
				</div>	

				<section>
					<label class="label">
						*Harap segera melakukan konfirmasi setelah mengisi form ini<br/><br/>
						**Informasi mengenai cara konfirmasi akan dikirim ke email Anda<br/><br/>
						***Jika Anda tidak menemukan email dalam kotak masuk, harap periksa kotak spam Anda
					</label>
				</section>

				<section>
					<label><input name="req1" id="creq1" required="" type="checkbox"><i></i> 1. Dengan ini saya menyatakan bahwa data di atas adalah benar.</label><br/>

					<div id="req2" style="display: none;"><label><input name="req2" id="creq2" required="" type="checkbox"><i></i> 2. Saya berjanji akan menggunakan proyektor dengan bijaksana.</label><br/></div>

					<div id="req3" style="display: none;"><label><input name="req3" id="creq3" required="" type="checkbox"><i></i> 3. Saya bersedia bertanggung jawab terhadap peminjaman proyektor sepenuhnya.</label><br/></div>

					<div id="req4" style="display: none;"><label><input name="req4" id="creq4" required="" type="checkbox"><i></i> 4. Saya akan mengembalikan secara utuh dan bersih barang yang dipinjam.</label><br/></div>

					<div id="req5" style="display: none;"><label><input name="req5" id="creq5" required="" type="checkbox"><i></i> 5. Jika terjadi kerusakan atau hilangnya peralatan yang dipinjam, saya bersedia untuk mengganti peralatan tersebut dengan yang baru atau sesuai dengan kesepakan yang akan disepakati sesudahnya.</label><br/></div>

					<div id="req6" style="display: none;"><label><input name="req6" id="creq6" required="" type="checkbox"><i></i> 6. Saya bersedia dikenakan denda sebesar Rp. 50.000/hari apabila peminjaman melebihi batas waktu pengembalian yang telah ditentukan (Max H+1 setelah peminjaman).</label><br/></div>
				</section>
					<section>
			<div class="g-recaptcha" data-sitekey="6LfHqkEUAAAAAB0QdOVCFtk2JJD9ngDXKCBPFdPj"></div>
			</section>
			</fieldset>
			<footer>
				<input type="submit" name="sewa" class="button" value="Sewa"/>
			</footer>
		</form>
			
	</div>
<script>
$(document).ready(function () {
	$("#type").change(function () {
		var val = $(this).val();
		if (val == "Internal") {
			$("#internal").show("slow");
			$("#internal :input").prop('required',true);
			$("#internal :input").prop('disabled',null);
			$("#eksternal").hide("slow");
			$("#eksternal :input").prop('required',null);
			$("#eksternal :input").prop('disabled',true);
		} else if (val == "Eksternal") {
			$("#eksternal").show("slow");
			$("#eksternal :input").prop('required',true);
			$("#eksternal :input").prop('disabled',null);
			$("#internal").hide("slow");
			$("#internal :input").prop('required',null);
			$("#internal :input").prop('disabled',true);
		}
	});

	$("#datepickerint").change(function () {
		$("#durasiint").prop('disabled',null);
	});

	$("#datepickerext").change(function () {
		$("#durasiext").prop('disabled',null);
	});

	$('#creq1').change(function() {
	    if($('#creq1').is(':checked')) {
	        $("#req2").show("slow");
	    }else{
	    	$("#req2").hide("slow");
	    }
	});

	$('#creq2').change(function() {
	    if($('#creq2').is(':checked')) {
	        $("#req3").show("slow");
	    }else{
	    	$("#req3").hide("slow");
	    }
	});

	$('#creq3').change(function() {
	    if($('#creq3').is(':checked')) {
	        $("#req4").show("slow");
	    }else{
	    	$("#req4").hide("slow");
	    }
	});

	$('#creq4').change(function() {
	    if($('#creq4').is(':checked')) {
	        $("#req5").show("slow");
	    }else{
	    	$("#req5").hide("slow");
	    }
	});

	$('#creq5').change(function() {
	    if($('#creq5').is(':checked')) {
	        $("#req6").show("slow");
	    }else{
	    	$("#req6").hide("slow");
	    }
	});

});

$(function () {
   $( "#durasiint" ).change(function() {
      var max = parseInt($(this).attr('max'));
      var min = parseInt($(this).attr('min'));
      if ($(this).val() > max)
      {
          $(this).val(max);
          hitungint();
      }
      else if ($(this).val() < min)
      {
          $(this).val(min);
          hitungint();
      }       
    }); 
});

$(function () {
   $( "#durasiext" ).change(function() {
      var max = parseInt($(this).attr('max'));
      var min = parseInt($(this).attr('min'));
      if ($(this).val() > max)
      {
          $(this).val(max);
          hitungext();
      }
      else if ($(this).val() < min)
      {
          $(this).val(min);
          hitungext();
      }       
    }); 
});

function hitungint(){
	var val = $('#durasiint').val();

	var from = $("#datepickerint").val().split("-");
	var i = (val*1 + 1);
	var f = new Date(from[2], from[1], from[0]);
	var back = f.setDate(f.getDate() + i);
	var back = new Date(back);
	
	var month = back.getUTCMonth();
	var day = back.getUTCDate();
	var year = back.getUTCFullYear();
	var backDate = day + "-" + month + "-" + year;

	$("#tglbackint").val(backDate);
};

function hitungext(){
	var val = $('#durasiext').val();
	var jum = val * 50000;
	var jumx = jum;
	//jum = jum.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	var jum = jum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	$("#biayaext").val("Rp "+jum+",-");
	$("#biayaext").val(jum);

	var from = $("#datepickerext").val().split("-");
	var i = (val*1 + 1);
	var f = new Date(from[2], from[1], from[0]);
	var back = f.setDate(f.getDate() + i);
	var back = new Date(back);
	
	var month = back.getUTCMonth();
	var day = back.getUTCDate();
	var year = back.getUTCFullYear();
	var backDate = day + "-" + month + "-" + year;

	$("#tglbackext").val(backDate);
};
</script>
</body>
</html>
