<?php
session_start();
require_once '../db.php';

if(isset($_POST['login-proyektor'])){
	if($_POST['username'] == ''){
		echo '<script>alert("Harap isi email Anda dengan benar")</script>';
		echo '<script language="javascript">window.location.href = "./?login=false"</script>';
	}
	if($_POST['password'] == ''){
		echo '<script>alert("Harap isi password Anda dengan benar")</script>';
		echo '<script language="javascript">window.location.href = "./?login=false"</script>';
	}

	$username = $_POST['username'];
	$password = $_POST['password'];
	/*
	$cek_login=$mysqli->query("SELECT id,username,nama FROM administrator WHERE username = '$username' AND password = '$password'");
	$cek_login_num=$cek_login->num_rows;
	*/
	$xray = 0;
	if($username == 'admin'){
		$xray++;
	}else{
		header("Location: ./?login=false");
	}

	if($password == 'kataproyektor'){
		$xray++;
	}else{
		header("Location: ./?login=false");
	}

	if($xray == 2){
		$_SESSION['proyektor-admin'] = true;
		header("Location: ./");
	}else{
		header("Location: ./?login=false");
	}
}

if(!isset($_SESSION['proyektor-admin'])){
	require_once 'login.php';
	die();
}

if(isset($_POST['saveFeeds'])){
	include 'save-feeds.php';
}

if(isset($_POST['verifikasi'])){
	$id = $_POST['id'];
	$status = 1;
	$query = $mysqli->prepare('UPDATE proyektor SET status = ? WHERE id = ?');
	$query->bind_param('ii', $status,$id);
	$query->execute();
	$query->close();
}

if(isset($_POST['hapus'])){
	$id = $_POST['id'];
	$query = $mysqli->prepare('DELETE FROM proyektor WHERE id =?');
	$query->bind_param('i', $id);
	$query->execute();
	$query->close();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin Panel - Proyektor</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.material.min.css">

<script src="//code.jquery.com/jquery-1.12.4.js"></script>

<style>
.mdl-data-table th{
	text-align: left !important;
}
.mdl-data-table td{
	text-align: left !important;
}
.mdl-button{
	min-width: 10px !important;
}
</style>

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><span>PROYEKTOR</span>ONLINE</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Admin <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="logout.php"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
			<li class="active"><a href="index.php"><i class="fa fa-fw fa-user"></i> Daftar Peminjam</a></li>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Daftar Peminjam</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<hr/>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<?php
					$biaya = 0;
					$result=$mysqli->query("SELECT biaya FROM proyektor WHERE status = 1");
					while($row = $result->fetch_array()){
						$biaya = $biaya + $row['biaya'];
					}
					echo '<input type="hidden" name="biayahidden" id="biayahidden" value="'.$biaya.'">';
					?>
					<script>
					$(document).ready(function () {
						var val = $('#biayahidden').val();
						//var jum = val;
						var jum = val * 1;
						var jum = jum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
						$("#pendapatan").text("Total Pendapatan Terverifikasi: Rp "+jum+",-");
					});
					</script>
					<div class="panel-heading">Table Data Peminjam Proyektor <div style="float:right;" id="pendapatan"></div></div>
					<div class="panel-body">
						<table id="table-peserta-scroll" class="mdl-data-table table-bordered table-hover nowrap" width="100%" cellspacing="0">
					        <thead>
					            <tr>
					                <th>No.</th>
					                <th>Kode</th>
					                <th>Nama</th>
					                <th>NPM</th>
					                <th>Email</th>
					                <th>Status</th>
					                <th>Penyewa</th>
					                <th>Keperluan</th>
					                <th>Jumlah</th>
					                <th>Tanggal Peminjaman</th>
					                <th>Durasi Peminjaman</th>
					                <th>Biaya</th>
					                <th>Tanggal Pengembalian</th>
					                <th>Status</th>
					                <th><center>Action</center></th>
					            </tr>
					        </thead>
					        <tbody>
					        <?php
					        $no = 0;
							$result=$mysqli->query("SELECT * FROM proyektor");
							while($row = $result->fetch_array()){
								$no++;
							?>
								<tr>
									<!-- No. Urut -->
									<td><?php echo $no; ?></td>
									<!-- Anggota Tim -->
									<td><?php echo $row['kode'] ?></td>
									<!-- Anggota Tim -->
									<td><?php echo $row['nama'] ?></td>
									<!-- NPM Anggota Tim -->
									<td><?php echo $row['npm'] ?></td>
									<!-- Status Foto Anggota Tim -->
									<td><?php echo $row['email'] ?></td>
									<!-- Status KRS Anggota Tim -->
									<td><?php echo $row['org'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['peminjam'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['keperluan'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['jumlah'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['tgl_pinjam'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['durasi_pinjam'] ?> Hari</td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['biaya'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php echo $row['tgl_kembali'] ?></td>
									<!-- Lomba Yang Diikuti -->
									<td><?php if($row['status']==1){echo 'Terverifikasi';}else{echo 'Belum diverifikasi';} ?></td>
									<td><center>
									<form action="" method="post" onsubmit="return confirm('Yakin ingin verifikasi status peminjaman ini?');">
									<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
									<input type="submit" name="verifikasi" value="Verifikasi" class="btn btn-primary">
									</form>
									<br/>
									<form action="" method="post" onsubmit="return confirm('Yakin ingin menghapus data peminjaman ini?');">
									<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
									<input type="submit" name="hapus" value="Hapus" class="btn btn-danger">
									</form>
									</center></td>
								</tr>
							<?php } ?>
						    </tbody>
    					</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		
		
	</div><!--/.main-->

    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.material.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
	<script>
	$(document).ready(function() {
	    $('#table-peserta-scroll').DataTable( {
	    	scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 1
            }
	    } );
	} );
	</script>
</body>

</html>