<?php
require_once('sesi.php');
if ($_SESSION['admin'] !== true) {
	goto selesai;
} else {
	if ($_POST && isset($_POST['unggah'])) {
		$sqlnya= "INSERT INTO `ebook_fikti_utama`(`judul`, `kategori`, `keterangan`, `link_download`) VALUES (?,?,?,?)";

		$stmt = $conn->prepare($sqlnya);
		$stmt->bind_param("ssss",$_POST['u1'],$_POST['u2'],$_POST['u3'],$_POST['u4']);
        if(!$stmt->execute()) { 
        	echo '<script>alert("kesalahan terjadi")</script>';
        }

     

	}
}
?>
<br>
<hr>
<div class="container">
  <h2>Admin menu</h2>
  <p>Silahkan anda isi yang anda perlukan :)</p>    
<form method="post" action="">
<div class="form-group">
  <label for="usr">Judul:</label>
  <input type="text" class="form-control" id="usr"  name="u1">
</div>
<div class="form-group">
  <label for="usr2">Kategori:</label>
  <input type="text" class="form-control" id="usr2"  name="u2">
</div>

<div class="form-group">
  <label for="usr3">Keterangan:</label>
  <input type="text" class="form-control" id="usr3"  name="u3">
</div>
<div class="form-group">
  <label for="usr4">Link Download:</label>
  <input type="text" class="form-control" id="usr4" name="u4">
</div>
<div class="form-group">
	<input type="submit" class="btn btn-primary" name="unggah" value="Simpan">
</div>
</form>


  <table class="table table-striped">
    <thead>
      <tr>
        <th>Judul</th>
        <th>Kategori</th>
        <th>Keterangan</th>
        <th>Link Download</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
   <?php
 				$sqlnya = "SELECT `id`, `judul`, `kategori`, `keterangan`, `link_download` FROM `ebook_fikti_utama`";
                $stmt = $conn->prepare($sqlnya);
                if($stmt->execute()) { 
                	 $stmt->bind_result($id,$judul,$kat,$ket,$lk);
    					while ($stmt->fetch()) {
    						?>

						<tr>
				        <td><?=$judul?></td>
				        <td><?=$kat?></td>
				        <td><?=$ket?></td>
				        <td><a href="<?=$lk?>">disini</a></td>
				        <td><a href="ubah.php?no=<?=$id?>">Ubah</a> - <a href="hapus.php?no=<?=$id?>">Hapus</a></td>
				      </tr> 
				      <?php
    					}
                }
      
   ?> 	
     
    </tbody>
  </table>
</div>
<?php
selesai:
?>