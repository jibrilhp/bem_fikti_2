<?php

require_once('sesi.php');
require_once('init.php');

$sqlnyaa ="SELECT `judul`, `kategori`, `keterangan`, `link_download` FROM `ebook_fikti_utama` WHERE id=?";

if (isset($_GET['no']) && is_numeric($_GET['no'])) {
	if (isset($_SESSION['admin']) && $_SESSION['admin'] === true) {

	if (!$_POST) {
				$stmt = $conn->prepare($sqlnyaa);

                $stmt->bind_param("s",$_GET['no']);
                if($stmt->execute()) { 
                	 $stmt->bind_result($jdl,$kat,$ket,$lk);
    					while ($stmt->fetch()) {
    						$judul = $jdl;
    						$keterangan = $ket;
    						$kategori = $kat;
    						$link = $lk;

    					}
                }
		} else { //else  apakah dia post/ proses simpan?
			$sqlnyaa ="UPDATE `ebook_fikti_utama` SET `judul`=?,`kategori`=?,`keterangan`=?,`link_download`=? WHERE id=?";
			$stmt = $conn->prepare($sqlnyaa);

                $stmt->bind_param("sssss",$_POST['u1'],$_POST['u2'],$_POST['u3'],$_POST['u4'],$_GET['no']);
                if($stmt->execute()) { 
                	$pesan = '	<div class="alert alert-success">
  								<strong>Berhasil!</strong> Berhasil mengubah data, <a href="./">klik ini untuk kembali</a>
								</div>';
								$judul = $_POST['u1'];
    						$keterangan = $_POST['u3'];
    						$kategori = $_POST['u2'];
    						$link = $_POST['u4'];

                } else {
                	$pesan ='<div class="alert alert-danger">
  								<strong>Gagal!</strong> Kesalahan terjadi, <a href="./">klik ini untuk kembali</a>
								</div>';
                }
                


		} // tutup else yang post
	} else {
	
	echo '403';
	exit();
	}
} else {
	header("Location: meong");
	echo 'no hekel please';
	exit();
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

  <h2>Mengubah bagian...</h2>
  <?php
if (isset($pesan)) {
	echo $pesan;
}
  ?>
  <form method="post" action="">
<div class="form-group">
  <label for="usr">Judul:</label>
  <input type="text" class="form-control" id="usr" value="<?=$judul?>" name="u1">
</div>
<div class="form-group">
  <label for="usr2">Kategori:</label>
  <input type="text" class="form-control" id="usr2" value="<?=$kategori?>" name="u2">
</div>

<div class="form-group">
  <label for="usr3">Keterangan:</label>
  <input type="text" class="form-control" id="usr3" value="<?=$keterangan?>" name="u3">
</div>
<div class="form-group">
  <label for="usr4">Link Download:</label>
  <input type="text" class="form-control" id="usr4" value="<?=$link?>" name="u4">
</div>
<div class="form-group">
	<input type="submit" class="btn btn-primary" name="unggah" value="Simpan">
</div>
</form>

</div> <!-- container -->

</body>
</html>