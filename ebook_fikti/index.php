<?php
require_once('init.php');
require_once('sesi.php');

//cek https atau bukan hehe -- 
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    echo 'pindah haluan, bisa baca? pentest sistem ini juga sabi hehe';
    exit();
}

//sekarang cek input, dia nyari apa ya?


//kalau sudah ketemu lempar ke tampil




?>

<!DOCTYPE html>
<html lang="id">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ebook FIKTI, memudahkan mahasiswa FIKTI untuk mencari e-book">
    <meta name="author" content="PTI 2017/2018">

    <title>E-Book FIKTI - BEM FIKTI UG</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">
<?php include ($_SERVER['DOCUMENT_ROOT'] . "/analisis.php"); ?>
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">E-book FIKTI</a>
        <?php
if ($_SESSION['admin'] !== false) {
          ?>
          <div class="pull-right">
          <a class="btn btn-primary" href="admin.php"><i class="fa fa-user-secret fa-lg fa-fw"></i> Admin</a>
          <?php 
        } 
if (isset($_SESSION)) {
  ?>
        <a class="btn btn-primary" href="./?ak=logout"><i class="fa fa-sign-out fa-lg fa-fw"></i>Logout</a>
        <?php
      } ?>
    </div>
      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">Cari judul atau sebuah kategori buku yang anda inginkan!</h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form method="post" action="">
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="text" name="cari" class="form-control form-control-lg" placeholder="* untuk semua, research, IT">
                </div>
                <div class="col-12 col-md-3">
                  <button type="submit" class="btn btn-block btn-lg btn-primary">Cari</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </header>
 
<?php
include("hasil.php");

?>

   
    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="../">Tentang </a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="../kerjasama">Kerjasama </a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="../aspirasi">Aspirasi BEM FIKTI</a>
              </li>
              
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&sdot; Maintained by BIRO PTI BEM FIKTI UG - 2018</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/bemfikti">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://twitter.com/ug_bemfikti">
                  <i class="fa fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.instagram.com/ug_bemfikti/">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
