<?php
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    echo 'pindah haluan, bisa baca? pentest sistem ini juga sabi hehe';
    exit();
}

if(session_id() == '') {
   session_start();                
   if (isset($_SESSION['sudah_masuk']) && $_SESSION['sudah_masuk'] === 'sip'){
                header('Location: ../');
                exit();
   }
    }
    //require_once('../sesi.php');
    if ($_POST) {
        sleep(1.5);
        if (isset($_POST['u']) && isset($_POST['p'])) {
            date_default_timezone_set("Asia/Jakarta");
            $hasil =file_get_contents("https://hf.ccug.gunadarma.ac.id/actions/studentsite.php?a=" . $_POST['u'] ."&p=" . $_POST['p']);
            $hhn = json_decode($hasil,true);
            if ($hhn['status'] === 'failed') {
                $yah = '
<div class="alert alert-danger">
  <strong>Maaf!</strong> username dan password anda salah
</div>';
            } else {
                //betul nah masuk ke jenjang session = oke..

                $tanggal = date("d-M-Y H i s");
                $_SESSION['sudah_masuk'] = 'sip';
                $_SESSION['npm'] = $hhn['npm'];
                $_SESSION['username'] = $hhn['username'];
                $_SESSION['email'] = $hhn['email'];
                $_SESSION['angkatan'] = $hhn['angkatan'];
                $_SESSION['nama'] = $hhn['nama'];

                require_once('../init.php');
                //insert ada 6  tapi admin pastikan 0 ya wkkw
                $sqlnya = "INSERT INTO `ebook_fikti_akun`( `nama`, `npm`, `angkatan`, `email`, `terakhir_akses`, `as_admin`) VALUES (?,?,?,?,?,0) ON DUPLICATE KEY UPDATE terakhir_akses=?";

                
                $stmt = $conn->prepare($sqlnya);

                $stmt->bind_param("ssssss",$hhn['nama'],$hhn['npm'],$hhn['angkatan'],$hhn['email'],$tanggal,$tanggal);
                if($stmt->execute()) { 
                    header('Location: ../');
                    exit();
                } else {
                    die('kesalahan system terjadi');
                }

            } //end if

        }
    }
?>
<!DOCTYPE html>
<html lang="id"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Memudahkan teman-teman menemukan Ilmu bersama BEM FIKTI UG Ebook FIKTI">
    <meta name="author" content="Arbha Pradana, Rizky Permana Putra, Jibril Hartri Putra">
    <link rel="icon" href="/favicon.ico">

    <title>Masuk ke Ebook FIKTI</title>

    <!-- Bootstrap core CSS -->
    <link href="index_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index_files/signin.css" rel="stylesheet">
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/analisis.php'); ?>
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method="post" action="">
        <h2 class="form-signin-heading">SSO Studentsite - Masuk</h2>

        <p>Silahkan masukkan username dan password dengan menggunakan studentsite anda</p>
        <?php if (isset($yah)) { echo $yah;} ?>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input id="inputEmail" class="form-control" placeholder="username" required="" autofocus="" type="text" name="u">
        <label for="inputPassword" class="sr-only">Password</label>
        <input id="inputPassword" class="form-control" placeholder="Password" required="" type="password" name="p">
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
      </form>

    </div> <!-- /container -->
	
	<div class="container">
	 <div class="alert alert-info">
	 <?php
	  require_once('../init.php');
	   $sqlnd = "SELECT nama_pencarian
FROM `ebook_fikti_stat`
ORDER BY `ebook_fikti_stat`.`id` DESC
LIMIT 1 ";

	  $stmt = $conn->prepare($sqlnd);
	if($stmt->execute()) { 
       $stmt->bind_result($namapenc);           
	  while($stmt->fetch()) {
	 ?>
    <strong>Aktivitas pencarian terakhir :</strong> <?=$namapenc?>
  </div>
	<?php
	  } //end while
	} 
	?>
	 <div class="alert alert-warning alert-dismissable">
	  <strong>Tahukah Anda?</strong> Email .student.gunadarma.ac.id dapat digunakan melalui website studentsite
	</div>
	
	</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
