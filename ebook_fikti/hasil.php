

<?php

function update_data_buku($id,$namafile)  {
  include("init.php");

  $sqlnya = "UPDATE `ebook_fikti_utama` SET `cover`=? WHERE `id`=?";
    $stmt = $conn->prepare($sqlnya);
    $stmt->bind_param("ss",$namafile,$id);
    //echo 'id'. $id;
    return ($stmt->execute());


}


$xx=0;
if (isset($_POST['cari'])) {

  ?>
  <div class="container">

      <!-- Page Heading -->
      <h1 class="my-4">Hasil Pencarian
        <small><?php if (isset($_POST['cari'])) {
		if (strlen($_POST['cari']) > 32) { echo 'kalimat terlalu panjang..'; } else {
		echo htmlentities($_POST['cari']);} } ?></small>
      </h1>
      <hr>
<?php
$amb = strval($_POST['cari']);

if ($amb === '*') {
	$sqlnya = "SELECT judul, kategori, keterangan,link_download,id,cover FROM `ebook_fikti_utama` WHERE 1";
} else {
	if (strlen($amb) > 32) { $amb = substr(0,32); }
$sqlnya = "SELECT judul, kategori, keterangan,link_download,id,cover FROM `ebook_fikti_utama` WHERE `judul` REGEXP ? or `kategori` REGEXP ? ";
}
    
    $stmt = $conn->prepare($sqlnya);
    $stmt->bind_param("ss",$amb,$amb);
    $stmt->execute();
    $stmt->bind_result($arg1,$arg2,$arg3,$arg4,$arg5,$arg6);
 ?>   


      <!-- proses pencarian -->
      <div class="row show-grid">
<?php
while ($stmt->fetch()) {
$xx++;


if (!isset($arg6)) {
//cek kalau nggak ada cover, maka segera download pdf dan generate gambarnya..
 $arg4x = str_replace("&amp;","&", $arg4);
 $arg4x = str_replace("&","@", $arg4x);
 //die($arg4x);
$ambil = file_get_contents("http://belajar.ccug.gunadarma.ac.id/lihatpdf2.php?apa=".$arg4x."&key=PTI_Banget~");
$nama_file = hash("sha256","nama=".$arg1 . "link_download=" . $link_download .  rand(0, 10000000)) . ".jpg"; //supaya hashnya panjang aja wkwk
file_put_contents("img/gambar_cover/" . $nama_file, $ambil);

//sekarang proses update..
if (update_data_buku($arg5,"img/gambar_cover/" .$nama_file) === true) {
  echo '<!-- berhasil update --> ';
  $arg6= "img/gambar_cover/" . $nama_file;
} else {
  echo '<!-- galat on update -->';
}


}
?>      
   
  <div class="col-sm-5 col-md-3">
    <div class="thumbnail">
      <a class="thumbnail" href="detail.php?id=<?=$arg5?>">
      <img src="<?=$arg6?>" alt="..." style="height: 250px;width: 200px;">
      </a>
      <div class="caption" style="padding-top: 10px;">
        <h6><?=$arg1?></h6>
		<p><i>Kategori : <?=$arg2?></i></p>
        <p><i><?=$arg3?></i></p>
        <p><a href="detail.php?id=<?=$arg5?>" class="btn btn-primary" role="button">Detail</a></p>
        <!-- jangan lupa kips pakai analytics -->
         <p><a href="<?=$arg4?>" target="_blank" class="btn btn-primary" role="button" target="_blank">Unduh</a></p>
         <!-- pakainya new window target="_blank" -->
      </div>
    </div>
  </div>

         <?php
  } // END LOOP
  if ($xx === 0) {
    ?>
    <div class="alert alert-warning">
        <strong>Tidak Ditemukan!</strong> coba cari dengan kata kunci yang lain.. kami akan menambahkan kembali kebutuhan teman-teman, silahkan kunjungi lain waktu.
    </div>
    <?php
  }
  $sqlnya= "INSERT INTO `ebook_fikti_stat`(`nama_pencarian`, `waktu_pencarian`, `npm`) VALUES (?,?,?)";
 $stmt = $conn->prepare($sqlnya);
 $mencari = htmlentities($amb);
 $tanggal =date("d m Y H i s");
    $stmt->bind_param("sss",$mencari,$tanggal,$_SESSION['npm']);
    $stmt->execute();
}



?>

   
      <!-- /.row -->
</div>
      <hr>
	 </div>
   