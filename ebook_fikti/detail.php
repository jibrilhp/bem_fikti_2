<?php
require_once('init.php');
require_once('sesi.php');

//cek https atau bukan hehe -- 
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    echo 'pindah haluan, bisa baca? pentest sistem ini juga sabi hehe';
    exit();
}

//sekarang cek input, dia nyari apa ya?


//kalau sudah ketemu lempar ke tampil




?>

<!DOCTYPE html>
<html lang="id">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ebook FIKTI, memudahkan mahasiswa FIKTI untuk mencari e-book">
    <meta name="author" content="PTI 2017/2018">

    <title>E-Book FIKTI - BEM FIKTI UG</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="/ebook_fikti">E-book FIKTI</a>
        <?php
if (isset($_SESSION)) {
  ?>
        <a class="btn btn-primary" href="./?ak=logout">Logout</a>
        <?php
      } ?>
      </div>
    </nav>


<?php
  if (isset($_GET['id'])){
    $sql = "SELECT judul, kategori, keterangan,link_download,id,cover FROM `ebook_fikti_utama` WHERE `id` = ? ";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s",$_GET['id']);
    $stmt->execute();
    $stmt->bind_result($arg1,$arg2,$arg3,$arg4,$arg5,$arg6);

    while ($stmt->fetch()) {

?>
			<script>
		function Balik() {
			window.location="https://fikti.bem.gunadarma.ac.id/ebook_fikti";
		}
		</script>
    <!-- Masthead -->
    <header class="">
      <div class="container" style="padding-top: 50px;">
        <button onclick="Balik()" class="btn btn-info" role="button" >Kembali</button>
		<hr>
        <div class="row">  
		
         <div class="col-md-6 text-center">
          <img src="<?=$arg6?>" alt="<?=$arg1?>" class="image-responsive rounded" width="300px" height="450px">
         </div>         
         <div class="col-md-6" >           
          <div style="min-height: 450px;">             
              <div class="row">
                <div class="col-md-12">
                  <h4><?=$arg1?></h4>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-12">
                 <span class="label label-primary small">Kategori <i class="fa fa-arrow-right fa-xs fa-fw"></i></span>
                 <span class="monospaced text-capitalize small"><?=$arg2?></span>
                </div>
              </div><!-- end row -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <br>
                    <label for="keterangan">Deskripsi Buku</label>
                    <textarea class="form-control" rows="12" id="keterangan" disabled><?=$arg3?>
                    </textarea>
                  </div>
                </div>
              </div><!-- end row -->
          </div>
            <div class="row">
              <div class="col-md-12">
              <a href="<?=$arg4?>" target="_blank" class="btn btn-info btn-block" role="button">
                <i class="fa fa-download fa-lg fa-fw"></i> Unduh</a>
            </div>
         </div>
        </div><!-- end row -->
       </div><!-- end container -->
    </header>
 
<?php
  }
}
include('si_admin.php');
?>

   
    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="../">Tentang </a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="../kerjasama">Kerjasama </a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="../aspirasi">Aspirasi BEM FIKTI</a>
              </li>
              
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&sdot; Maintained by BIRO PTI BEM FIKTI UG - 2018</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/bemfikti">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://twitter.com/ug_bemfikti">
                  <i class="fa fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.instagram.com/ug_bemfikti/">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
