<?php

require_once('sesi.php');
require_once('init.php');

$sqlnya ="DELETE FROM `ebook_fikti_utama` WHERE `ebook_fikti_utama`.`id` = ?";

if (isset($_GET['no']) && is_numeric($_GET['no'])) {
	if (isset($_SESSION['admin']) && $_SESSION['admin'] === true) {
			$stmt = $conn->prepare($sqlnya);

                $stmt->bind_param("s",$_GET['no']);
                if($stmt->execute()) { 
                	echo '<script>alert("berhasil dihapus");window.open("https://fikti.bem.gunadarma.ac.id/ebook_fikti","_parent");</script>';

                } else {
                	echo '<script>alert("gagal dihapus");window.open("https://fikti.bem.gunadarma.ac.id/ebook_fikti","_parent");</script>';
                }
		}
	}

?>